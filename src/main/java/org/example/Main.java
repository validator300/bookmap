package org.example;

import java.io.*;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class Main {

    static class Order {
        int price;
        int size;

        public Order(int price, int size) {
            this.price = price;
            this.size = size;
        }
    }

    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]));
             BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt"))) {
            NavigableMap<Integer, Integer> bids = new TreeMap<>((a, b) -> b - a);
            NavigableMap<Integer, Integer> asks = new TreeMap<>();

            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                String command = parts[0];

                switch (command) {
                    case "u":
                        int price = Integer.parseInt(parts[1]);
                        int size = Integer.parseInt(parts[2]);
                        String type = parts[3];
                        if ("bid".equals(type)) {
                            updateOrder(bids, price, size);
                        } else if ("ask".equals(type)) {
                            updateOrder(asks, price, size);
                        }
                        break;
                    case "q":
                        String queryType = parts[1];
                        switch (queryType) {
                            case "best_bid":
                                if (!bids.isEmpty()) {
                                    int bestBidPrice = bids.firstKey();
                                    int bestBidSize = bids.get(bestBidPrice);
                                    writer.write(bestBidPrice + "," + bestBidSize);
                                    writer.newLine();
                                }
                                break;
                            case "best_ask":
                                if (!asks.isEmpty()) {
                                    int bestAskPrice = asks.firstKey();
                                    int bestAskSize = asks.get(bestAskPrice);
                                    writer.write(bestAskPrice + "," + bestAskSize);
                                    writer.newLine();
                                }
                                break;
                            case "size":
                                int targetPrice = Integer.parseInt(parts[2]);
                                int targetSize = getSizeAtPrice(bids, targetPrice) + getSizeAtPrice(asks, targetPrice);
                                writer.write(String.valueOf(targetSize));
                                writer.newLine();
                                break;
                        }
                        break;
                    case "o":
                        String orderType = parts[1];
                        int orderSize = Integer.parseInt(parts[2]);
                        if ("buy".equals(orderType)) {
                            removeMarketOrders(asks, orderSize);
                        } else if ("sell".equals(orderType)) {
                            removeMarketOrders(bids, orderSize);
                        }
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void updateOrder(NavigableMap<Integer, Integer> orders, int price, int size) {
        if (size == 0) {
            orders.remove(price);
        } else {
            orders.put(price, size);
        }
    }

    private static void removeMarketOrders(NavigableMap<Integer, Integer> orders, int size) {
        for (Map.Entry<Integer, Integer> entry : orders.entrySet()) {
            int price = entry.getKey();
            int orderSize = entry.getValue();
            if (size >= orderSize) {
                size -= orderSize;
                orders.remove(price);
            } else {
                entry.setValue(orderSize - size);
                break;
            }
        }
    }

    private static int getSizeAtPrice(NavigableMap<Integer, Integer> orders, int price) {
        Integer size = orders.get(price);
        return size != null ? size : 0;
    }
}